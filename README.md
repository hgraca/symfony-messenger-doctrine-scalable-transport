Symfony Messenger Doctrine Scalable Transport
==================

Provides Doctrine integration for Symfony Messenger.

This is a clone of the default Symfony Doctrine transport, with some extra features and capabilities 
that make it scalable.

I will periodically open pull requests, with these changes, to the Symfony repo.

Extra features
---------

- SKIP LOCKED
      
   With newer DB versions, we can use `SKIP LOCKED` to have several workers pulling messages from the same 
   messages table, without interfering with each other. Basically, one worker pulls messages and locks them, 
   using `READ FOR UPDATE`, until its work is done. Another worker will also pull messages, but it will ignore the 
   rows locked by the other worker(s), thanks to the `SKIP LOCKED`.
   This is enabled by default, there's nothing we need to do to use it, and we can not disable it.


- batch_size
      
   How many messages a consumer will pull in one go from the queue and handle, before pulling more messages.
   Default value: 1
   We can use it by adding the option to the transport DSN config.

   Ex:

  `'commands' => "doctrine://primary?table_name=messenger_outbox&queue_name=commands&batch_size=50",`

Resources
---------

 * [Contributing](https://symfony.com/doc/current/contributing/index.html)
 * [Report issues](https://github.com/symfony/symfony/issues) and
   [send Pull Requests](https://github.com/symfony/symfony/pulls)
   in the [main Symfony repository](https://github.com/symfony/symfony)
