CHANGELOG
=========

 * Add support for the doctrine transport to pull a batch of messages in one go (`batch_size` option)
 * Use `SKIP LOCKED` in the doctrine transport for MySQL, PostgreSQL and MSSQL

5.1.0
-----

 * Introduced the Doctrine bridge.
 * Added support for PostgreSQL `LISTEN`/`NOTIFY`.
